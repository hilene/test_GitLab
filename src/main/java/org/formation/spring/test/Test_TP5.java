package org.formation.spring.test;

import java.util.List;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.service.IPrestiBanqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test_TP5 {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);

		IPrestiBanqueService service = context.getBean("service", IPrestiBanqueService.class);
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>" + service.listClients());
//		Client c1 = new Client("jhii", "hgygyg", "ghyjuh", "hjjuhiuh", new Adresse(545,"hkjh","ghjhuj"));
//		Client c22 = new Client("BOB", "hgygyg", "ghyjuh", "hjjuhiuh",  new Adresse(545,"hkjh","ghjhuj"));
		Client c2 = new Client();
		c2.setNom("kkhjkhjjk");
//		service.addClient(c1);
		service.addClient(c2);
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>" + service.listClients());

		List<Client> resultat = service.chercherClients("kkhjkhjjk");
		System.out.println("________________________" + resultat);
		((ConfigurableApplicationContext) (context)).close();

	}

}
